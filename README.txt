# g4p_presentation #

这是为狗屎皮大会做的一个关于Fabric和Puppet的介绍。

这个演示是用ipython notebook 制作的。制作的过程中用到下面一些命令：

# ipython nbconvert Fabric_And_Puppet.ipynb --to slides

# git clone https://github.com/hakimel/reveal.js.git
# cd reveal.js
# git checkout 2.6.2

另外，还要在本地起一个web server，用下面的命令：
# python -m SimpleHTTPServer

这样，可以访问http://localhost:8000/Fabric_And_Puppet.slides.html 来观看。