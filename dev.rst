=================
Fabric and Puppet
=================

Stefan Zweig (stefan.zweig@gmail.com)

2015-06-23

Fabric
======
intro: Fabric is a Python (2.5-2.7) library and command-line tool for streamlining the use of SSH for application deployment or systems administration tasks.

link: http://www.fabfile.org/

scenario
--------

fabfile
-------

how to specify the hosts?
-------------------------

inside the source
-----------------


Puppet
======
